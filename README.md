# vzsaml-get-aws-credentials-auto.py

## DISCLAIMER: This is a heavily modified script copied from https://gitlab.com/vznbi/NBI-Shared-Showcase/vzawsfed
This is not up to date, and some functionality added to this script has already been added to the original. (and probably done much better than me...)

This script does shamelessly use touchid which is located https://github.com/lukaskollmer/python-touch-id. It's contents are directly in the script to simplify things.


## Add password to your mac keychain MAC ONLY

`vzsaml-get-aws-credentials-auto.py --username USWIN_NAME --set`
This will prompt for your password and store it in your mac keychain. It will then attempt to use this password for the user specified first. 

## This script authenticates to the Verizon Siteminder SSO site, using USWIN credentials.

Once authenticated, user is presented with the accounts and AWS roles he/she is authorized to assume.

Selecting one will grant access to that role for **one hour**.

