#!/usr/bin/env python3
"""
#--------------------------------------------------------------------------------------------------
#
#  Component:  vzsaml-get-aws-credentials-auto.py
#     Author:  Ken LeGro
#  Copyright:  Verizon Security Services, (c) 2020
#
#--------------------------------------------------------------------------------------------------
#
#  DESCRIPTION
#  -----------
#  The purpose of this program is generate a set of temporary access credentials which will enable
#  the user to execute commands using the AWS CLI.  The logic works as follows:
#
#     1.  Parse the command-line arguments.
#     2.  Unset any HTTP proxy settings.  The initial authertication call to Verizon's SSO must be
#         made without using an HTTP proxy.
#     3.  Prompt the user to enter their SSO password.
#     4.  Log the user into SSO and retrieve back the user's AWS SAML assertion. The SAML assertion
#         contains the list of federated AWS Roles which the user is allowed to access.
#     5.  Prompt the user to select one of the federated AWS Roles.
#     6.  If necessary, prompt the user to select an HTTP proxy. The user can also specify the
#         HTTP proxy via a command-line argument.  An HTTP proxy is needed in order to send a
#         request to the AWS STS endpoint which lives outside of Verizon's network.
#     7.  Invoke STS to retrieve a set of temporary access keys  which will enable the user to
#         access AWS using the selected Role.
#     8.  Store the temporary access keys in the user's AWS credentials file. Using a command-line
#         argument, the user can specify a section name where the access keys will be stored. This
#         allows users to store access keys for more than one Role. If no section name is specified
#         then the access keys are stored in the "default" section inside the credentials file.
#
#  FOR USAGE RUN
#  -------------
#  ./vzsaml-get-aws-credentials-auto.py --help
#
#--------------------------------------------------------------------------------------------------
"""

# Import standard modules
import argparse
import base64
import configparser
import getpass
import logging
import os
from os.path import expanduser
from pathlib import Path
import re
import sys
import xml.etree.ElementTree as ET

# Import add-on modules
import boto3
from botocore.exceptions import ClientError
from bs4 import BeautifulSoup
import requests
from tzlocal import get_localzone
import keyring

# TouchID Stuffs
import sys
import ctypes
from LocalAuthentication import LAContext
from LocalAuthentication import LAPolicyDeviceOwnerAuthenticationWithBiometrics

# Global variables
AWS_ACCOUNT_LIST = {
    "848091728933": ["vz-masterpayer-04"],
    "544060102515": ["vz-mp04-sb-iv0v-sodev"],
    "586103725137": ["vz-mp04-st-iv0v-sotest"],
    "757206425301": ["vz-mp04-pl-iv0v-sobeta"],
    "742758330894": ["vz-mp04-pr-iv0v-soprod"],
    "407819024145": ["vz-mp04-sb-i6jv-dsdev", "dsdev"],
    "471026160843": ["vz-mp04-st-i6jv-dstest", "dstest"],
    "573127458832": ["vz-mp04-pl-i6jv-dsbeta", "dsbeta"],
    "018582391531": ["vz-mp04-pr-i6jv-dsprod", "dsprod"],
    "469192624471": ["vz-mp04-st-h9rv-pltest","test"],
    "277983822432": ["vz-mp04-pr-ixtv-shared", "shared"],
    "304870922334": ["vz-mp04-sb-irzv-siccsw"],
    "930231627038": ["vz-mp04-sb-iqxv-sandbx"],
    "243391319359": ["vz-mp04-pr-czov-prod"],
    "629851624602": ["vz-mp04-np-ikvv-scdh"],
    "143820761469": ["vz-mp04-np-czov-techdv"],
    "274001302959": ["vz-mp04-sb-czov-sandbx"],
    "659056928757": ["vz-mp04-pr-iwiv-senion", "senion"],
    "423868553974": ["vz-mp04-logging"],
    "255646620369": ["vz-mp04-sb-czov-nonprd"],
    "214478391135": ["vz-mp04-np-il2v-5gd"],
    "945653131091": ["vz-mp04-pr-h9rv-pltfrm","prod", "beta"],
    "245028536754": ["vz-mp04-sb-h9rv-devsbx"],
    "246834354337": ["vz-mp04-np-ixtv-devops", "sandbox"],
    "755203376857": ["vz-mp04-pr-cypv-mapqst", "classic", "mapquest"],
    "521272022809": ["vz-mp04-pr-ixtv-innov8"],
    "012923621625": ["vz-it-np"],
    "628345350518": ["closed-account"],
    "004107182492": ["closed-account"],
    "273180043790": ["closed-account"]
}

AWS_ROLE_LIST = [
    "Administrator",
    "OnShoreDeveloper",
]

HTTP_PROXY_DICT = {}

SSO_URL = 'https://login.verizon.com/onesso/login.fcc'
TARGET_URL = 'https://login.verizon.com/onesso/resources/chkprofile.jsp?'\
    'ssoFinalTarget=https://federation.verizon.com/affwebservices/public/saml2sso?SPID=urn:amazon:webservices'

LOG = logging.getLogger(__name__)

VZ_PROXY_LIST = [
    'fhdcproxy.verizon.com',
    'idcproxy.us1.ent.verizon.com',
    'proxy.ebiz.verizon.com',
    'sacproxy.verizon.com',
    'server.proxy.vzwcorp.com',
    'tdcproxy.ebiz.verizon.com',
    'tpaproxy.verizon.com'
    ]

REGION_LIST = [
    'us-east-1',
    'us-west-2'
    ]

## TouchID stuffs

kTouchIdPolicy = LAPolicyDeviceOwnerAuthenticationWithBiometrics

c = ctypes.cdll.LoadLibrary(None)

PY3 = sys.version_info[0] >= 3
if PY3:
    DISPATCH_TIME_FOREVER = sys.maxsize
else:
    DISPATCH_TIME_FOREVER = sys.maxint

dispatch_semaphore_create = c.dispatch_semaphore_create
dispatch_semaphore_create.restype = ctypes.c_void_p
dispatch_semaphore_create.argtypes = [ctypes.c_int]

dispatch_semaphore_wait = c.dispatch_semaphore_wait
dispatch_semaphore_wait.restype = ctypes.c_long
dispatch_semaphore_wait.argtypes = [ctypes.c_void_p, ctypes.c_uint64]

dispatch_semaphore_signal = c.dispatch_semaphore_signal
dispatch_semaphore_signal.restype = ctypes.c_long
dispatch_semaphore_signal.argtypes = [ctypes.c_void_p]


def touchid_is_available():
    context = LAContext.new()
    return context.canEvaluatePolicy_error_(kTouchIdPolicy, None)[0]


def touchid_authenticate(reason='authenticate via Touch ID'):
    context = LAContext.new()

    can_evaluate = context.canEvaluatePolicy_error_(kTouchIdPolicy, None)[0]
    if not can_evaluate:
        raise Exception("Touch ID isn't available on this machine")

    sema = dispatch_semaphore_create(0)

    # we can't reassign objects from another scope, but we can modify them
    res = {'success': False, 'error': None}

    def cb(_success, _error):
        res['success'] = _success
        if _error:
            res['error'] = _error.localizedDescription()
        dispatch_semaphore_signal(sema)

    context.evaluatePolicy_localizedReason_reply_(kTouchIdPolicy, reason, cb)
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER)

    if res['error']:
        raise Exception(res['error'])

    return res['success']

#####

def get_credentials_file_name():
    """
    Get the name of the user's credentials file. Make sure the file's directory exists
    and that the file exists just in case the user has never run "aws configure".

    :returns: The name of the user's AWS credentials file.

    """
    homeDirectory = expanduser("~")

    if os.name == 'nt':
        LOG.debug("Windows OS")
        awsCredentialsDirName = homeDirectory + '\\.aws'
        awsCredentialsFileName = homeDirectory + '\\.aws\\credentials'
    else:
        LOG.debug("Unix OS")
        awsCredentialsDirName = homeDirectory + '/.aws'
        awsCredentialsFileName = homeDirectory + '/.aws/credentials'
    LOG.debug('AWS credentials file: %s', awsCredentialsFileName)

    try:
        dirPath = Path(awsCredentialsDirName)
        dirPath.mkdir(mode=0o755, exist_ok=True)
    except (OSError, FileNotFoundError) as thisError:
        print("")
        LOG.error('Unable to create .aws directory %s: %s', awsCredentialsDirName, thisError)
        LOG.error('Please try running "aws configure" before using this program')
        sys.exit(1)

    try:
        filePath = Path(awsCredentialsFileName)
        filePath.touch(mode=0o600, exist_ok=True)
    except (OSError, FileNotFoundError) as thisError:
        print("")
        LOG.error('Unable to create credentials file %s: %s', awsCredentialsFileName, thisError)
        LOG.error('Please try running "aws configure" before using this program')
        sys.exit(1)

    return awsCredentialsFileName


def store_credentials(awsCredentialsFileName, credentials, region, section):
    """
    Store the temporary credentials.

    :param param1: The name of the user's AWS credentials file.
    :param param2: A tuple containing the the temporary access credentials.
    :param param3: The name of the AWS region.
    :param param4: The name of the section where the credentials will be stored.

    :returns: A tuple containing the temporary access credentials.

    """
    # Read in the existing config file
    config = configparser.RawConfigParser()
    config.read(awsCredentialsFileName)

    # Setup section in config file
    if config.has_section(section):
        LOG.debug('Updating existing section %s', section)
    else:
        LOG.debug('Adding new section %s', section)
        config.add_section(section)

    # Write the AWS STS token into the AWS credential file
    config.set(section, 'output', 'json')
    config.set(section, 'region', region)
    config.set(section, 'aws_access_key_id', credentials[0])
    config.set(section, 'aws_secret_access_key', credentials[1])
    config.set(section, 'aws_session_token', credentials[2])

    # Write the updated config file
    with open(awsCredentialsFileName, 'w+') as awsCredentialsFile:
        config.write(awsCredentialsFile)

    def print_deets():
        # Give the user some basic info as to what has just happened
        print('''---------------------------------------------------------------------------------------------------------
    Your temporary access keys have been stored in your credentials file: {credentialsfile}
    '''.format(credentialsfile=awsCredentialsFileName), end='')

        if section != 'default':
            print('''
    To use these credentials append "--profile {section}" to your CLI commands.
    '''.format(section=section), end='')

        print('''
    These credentials will expire at {expire} but you can renew them at any time.
    ---------------------------------------------------------------------------------------------------------
    '''.format(expire=credentials[3].astimezone(get_localzone()).isoformat()), end='')

    #print_deets()

def get_temporary_aws_credentials(federatedRoleInfo, region, duration):
    """
    Get the STS credentials using the federated Role information.

    :param param1: A tuple containing the AWS role information.
    :param param2: The name of the AWS region.
    :param param3: The duration (in seconds) before the access credentials will expire.

    :returns: A tuple containing the temporary access credentials.

    """
    LOG.debug("Role %s, Principal %s", federatedRoleInfo[0], federatedRoleInfo[1])


    try:
        # Get temporary credentials from STS
        conn = boto3.client('sts', region_name=region)
        token = conn.assume_role_with_saml(RoleArn=federatedRoleInfo[0], PrincipalArn=federatedRoleInfo[1],
                                           SAMLAssertion=federatedRoleInfo[2], DurationSeconds=duration)
    except (ClientError) as thisError:
        print("")
        LOG.error('Unable to assume Role %s: %s', federatedRoleInfo[0], thisError)
        sys.exit(1)

    LOG.debug("%s", token['Credentials']['AccessKeyId'])
    LOG.debug("%s", token['Credentials']['SecretAccessKey'])
    LOG.debug("%s", token['Credentials']['SessionToken'])

    return (token['Credentials']['AccessKeyId'],
            token['Credentials']['SecretAccessKey'],
            token['Credentials']['SessionToken'],
            token['Credentials']['Expiration'])


def saml_role_sort_key(samlRole):
    """
    Create a key for sorting the SAML Role list.

    :param param1: The SAML Role.

    :returns: The sort key.

    """
    chunks = samlRole.split(',')
    roleName = chunks[0].split('/')[1]
    accountNumber = chunks[0].split(':')[4]
    return roleName + accountNumber


def get_saml_role_list(assertion):
    """
    Get a list of SAML Roles from the SAML assertion.

    :param param1: The SAML assertion XML.

    :returns: A list of SAML Roles

    """
    LOG.debug('Decoded SAML assertion"\n%s', base64.b64decode(assertion))

    # Parse the returned assertion and extract the authorized roles
    samlRoleList = []
    root = ET.fromstring(base64.b64decode(assertion))
    for samlAttribute in root.iter('{urn:oasis:names:tc:SAML:2.0:assertion}Attribute'):
        if samlAttribute.get('Name') == 'https://aws.amazon.com/SAML/Attributes/Role':
            for samlAttributeValue in samlAttribute.iter('{urn:oasis:names:tc:SAML:2.0:assertion}AttributeValue'):
                samlRoleList.append(samlAttributeValue.text)

    # Check the Role list to ensure that each entry contains "roleArn,principalArn"
    for samlRole in samlRoleList:
        LOG.debug("SAML Role = %s", samlRole)
        chunks = samlRole.split(',')
        if 'saml-provider' not in chunks[1]:
            samlRoleList.remove(samlRole)
            LOG.error("A saml-provider was not found for Role %s", samlRole)

    # Sort the Role list
    LOG.debug("Sorting Role list")
    sortedSamlRoleList = sorted(samlRoleList, key=saml_role_sort_key)

    # Dump the list of SAML Roles
    for samlRole in sortedSamlRoleList:
        LOG.debug("SAML Role = %s", samlRole)

    return sortedSamlRoleList


def get_federated_aws_role_info_INPUT(assertion, shortcut):
    """
    Get the federated AWS Role information from the SAML assertion by input flag.

    :param param1: The SAML assertion XML.

    :param param2: The alias name for the account.

    :returns: A tuple containing the AWS role information.

    """
    # Get SAML Role list
    sortedSamlRoleList = get_saml_role_list(assertion)

    index = 1
    #print("input = %s" % shortcut)
    accountFound = False
    availList = []
    while accountFound == False:
        for samlRole in sortedSamlRoleList:
            # for each acct number in manual list..
            for key in AWS_ACCOUNT_LIST:
                accountPattern = re.compile(key)
                # if the account number in the list is in the available AND if the input shortcut matches up with manual list save the index number and move on
                if accountPattern.search(samlRole) and shortcut in AWS_ACCOUNT_LIST[key]:
                    optionItem = {}
                    optionItem["selectedRoleIndex"] = index
                    optionItem["samlRole"] = samlRole
                    optionItem["selectedAcct"] = AWS_ACCOUNT_LIST[key][0]
                    optionItem["accountNumber"] = key
                    optionItem["desc"] = '%s, Account = %s' % (samlRole.split(',')[0].split('/')[1], AWS_ACCOUNT_LIST[key])
                    availList.append(optionItem)

                    index += 1
                    break

        #If more than one item in list let user choose, else select option
        if len(availList) > 1:
            print("Please choose the role you would like to assume:")
            for i in availList:
                print(get_ordered_index_print_pattern(i["selectedRoleIndex"]),i["desc"])

            print(get_ordered_index_print_pattern(len(availList) + 1), 'Quit')
            print("\nSelection: ", end='')
            selectedRoleIndex = int(input()) - 1
             # Bail out if the user wants to quit
            if selectedRoleIndex == len(availList):
                print("-")
                sys.exit(0)
            # Basic sanity check of user's selection
            if selectedRoleIndex < 0 or selectedRoleIndex > (len(availList)):
                print("-")
                LOG.error("ERROR: Sorry your selection was invalid, processing aborted")
                sys.exit(1)
            
            #chose selection from new list
            roleArn = availList[selectedRoleIndex]["samlRole"].split(',')[0]
            principalArn = availList[selectedRoleIndex]["samlRole"].split(',')[1]
        # If one result in list select it
        else:
            print(availList[0])
            roleArn = availList[0]["samlRole"].split(',')[0]
            principalArn = availList[0]["samlRole"].split(',')[1]

        accountFound = True

    print("Assuming Role %s" % (roleArn))


    return (roleArn, principalArn, assertion)


def get_federated_aws_role_info(assertion):
    """
    Get the federated AWS Role information from the SAML assertion.

    :param param1: The SAML assertion XML.

    :returns: A tuple containing the AWS role information.

    """
    # Get SAML Role list
    sortedSamlRoleList = get_saml_role_list(assertion)

    print("--")
    if len(sortedSamlRoleList) > 1:
        index = 1
        print("Please choose the role you would like to assume:")
        for samlRole in sortedSamlRoleList:
            accountFound = False
            for key in AWS_ACCOUNT_LIST:
                accountPattern = re.compile(key)
                if accountPattern.search(samlRole):
                    print(get_ordered_index_print_pattern(index) +
                          ('%s, Account = %s' % (samlRole.split(',')[0].split('/')[1], AWS_ACCOUNT_LIST[key])), key)
                    accountNumber = key
                    accountFound = True
                    break
            if not accountFound:
                print(get_ordered_index_print_pattern(index) +
                      ('%s, Account %s' % (samlRole.split(',')[0].split('/')[1], samlRole.split(':')[4])), key)
            index += 1
        print(get_ordered_index_print_pattern(index) + 'Quit')

        print("\nSelection: ", end='')

        selectedRoleIndex = int(input()) - 1

        # Bail out if the user wants to quit
        if selectedRoleIndex == len(sortedSamlRoleList):
            print("-")
            sys.exit(0)

        # Basic sanity check of user's selection
        if selectedRoleIndex < 0 or selectedRoleIndex > (len(sortedSamlRoleList) - 1):
            print("-")
            LOG.error("ERROR: Sorry your selection was invalid, processing aborted")
            sys.exit(1)

        roleArn = sortedSamlRoleList[selectedRoleIndex].split(',')[0]
        principalArn = sortedSamlRoleList[selectedRoleIndex].split(',')[1]
    else:
        roleArn = sortedSamlRoleList[0].split(',')[0]
        principalArn = sortedSamlRoleList[0].split(',')[1]

    print("Assuming Role %s, Account: %s" % (roleArn, accountNumber))

    return (roleArn, principalArn, assertion)


def extract_saml_assertion(loginResponse):
    """
    Extract the SAML assertion from the SSO response.

    :param param1: The login response returned by the SSO.

    :returns: The SAML assertion XML.

    """
    # Parse the HTML returned from the Login request
    parsedLoginResponse = BeautifulSoup(loginResponse, 'html.parser')
    assertion = None
    for inputTag in parsedLoginResponse.find_all(re.compile('(INPUT|input)')):
        if inputTag.get('name') == 'SAMLResponse':
            assertion = inputTag.get('value')
            break

    # Check that assertion was found
    if assertion is None:
        print("")
        LOG.error('The SSO response did not contain a SAML assertion')
        sys.exit(1)

    return assertion


def get_login_response_from_sso(username):
    """
    Get login response from SSO using form-based authentication.

    :param param1: The user's name in the SSO.

    :returns: The text response returned by the SSO after the login request was submitted.
    """

    # Use touchid to authenticate if not on windows.
    if os.name != 'nt':
        try: 
            touchid_is_available()

            if touchid_authenticate():
                password=keyring.get_password("vzawsfed",username)
            else:
                LOG.error("ERROR: Authentication Failed.")
                sys.exit(1)

        except Exception:
            password = getpass.getpass(prompt='\nPlease enter your AWS SSO password: ')
    else:
        password = getpass.getpass(prompt='\nPlease enter your AWS SSO password: ')

    # Define data to be sent
    payload = {'USERID': username, 'PASSWORD': password, 'Target': TARGET_URL}
    LOG.debug("SSO POST payload:\n%s", payload)

    # Overwrite and delete the credential variables, just for safety
    password = '##############################################'
    del password

    # Initiate HTTP session handler
    session = requests.Session()

    # Perform HTTP POST request against SSO URL
    loginResponse = session.post(SSO_URL, data=payload, verify=True)
    LOG.debug("Login response\n%s", loginResponse.text)

    return loginResponse.text


def get_ordered_index_print_pattern(index):
    """
    Get the index pattern for an ordered list.

    :param param1: The index value.

    :returns: The index print pattern.
    """
    if index < 10:
        return "[%d] " % index

    return "[%d] " % index


def unset_proxy_environment():
    """
    Unset any proxy environment variables.

    :returns: Nothing
    """
    proxyCount=0
    if os.getenv("HTTP_PROXY"):
        proxyCount += 1
        LOG.debug("Unsetting HTTP_PROXY")
        HTTP_PROXY_DICT["HTTP_PROXY"] = os.environ["HTTP_PROXY"]
        del os.environ["HTTP_PROXY"]
    if os.getenv("HTTPS_PROXY"):
        proxyCount += 1
        LOG.debug("Unsetting HTTPS_PROXY")
        HTTP_PROXY_DICT["HTTPS_PROXY"] = os.environ["HTTPS_PROXY"]
        del os.environ["HTTPS_PROXY"]
    if os.getenv("ALL_PROXY"):
        proxyCount += 1
        LOG.debug("Unsetting ALL_PROXY")
        HTTP_PROXY_DICT["ALL_PROXY"] = os.environ["ALL_PROXY"]
        del os.environ["ALL_PROXY"]    
    if os.getenv("http_proxy"):
        proxyCount += 1
        LOG.debug("Unsetting http_proxy")
        HTTP_PROXY_DICT["http_proxy"] = os.environ["http_proxy"]
        del os.environ["http_proxy"]
    if os.getenv("https_proxy"):
        proxyCount += 1
        LOG.debug("Unsetting https_proxy")
        HTTP_PROXY_DICT["https_proxy"] = os.environ["https_proxy"]
        del os.environ["https_proxy"]
    if os.getenv("all_proxy"):
        proxyCount += 1
        LOG.debug("Unsetting all_proxy")
        HTTP_PROXY_DICT["all_proxy"] = os.environ["all_proxy"]
        del os.environ["all_proxy"]
    print(proxyCount, "Proxies Unset")



def set_http_proxy(proxyString):
    """
    Set, or unset, the HTTP proxy to use for internet access.

    :param param1: (Optional) The HTTP proxy string to be used.

    :returns: Nothing.
    """
    if not proxyString:
        index = 1

        print("Please choose an HTTP proxy: ")
        for proxy in VZ_PROXY_LIST:
            print(get_ordered_index_print_pattern(index) + ('%s' % proxy))
            index += 1
        print(get_ordered_index_print_pattern(index) + ('%s' % 'Quit'))

        print("\nSelection: ", end='')

        selectedProxyIndex = int(input()) - 1

        # Bail out on Quit
        if selectedProxyIndex == len(VZ_PROXY_LIST):
            print("")
            sys.exit(0)
        if selectedProxyIndex < 0 or selectedProxyIndex > (len(VZ_PROXY_LIST) - 1):
            print("")
            LOG.error("ERROR: Your selection was invalid, processing aborted")
            sys.exit(1)

        proxyString = VZ_PROXY_LIST[selectedProxyIndex]

    # Set the HTTP Proxy port
    portPattern = re.compile(r'.+:\d{2,4}$')
    if not portPattern.match(proxyString):
        if proxyString == 'server.proxy.vzwcorp.com':
            proxyString = proxyString + ':9290'
        else:
            proxyString = proxyString + ':80'

    print("Using HTTP proxy {}".format(proxyString))
    os.environ["HTTP_PROXY"] = "http://" + proxyString
    os.environ["HTTPS_PROXY"] = "http://" + proxyString
    os.environ["http_proxy"] = "http://" + proxyString
    os.environ["https_proxy"] = "http://" + proxyString


def duration_arg_check(duration):
    """
    Check that a duration is between 3600 and 43200 seconds.

    :param param1: The duration string to be checked.

    :returns: The duration if the value is valid.

    :raises argparse.ArgumentTypeError: If the duration is invalid.
    """
    durationPattern = re.compile(r'^\d{4,5}$')
    if not durationPattern.match(duration):
        msg = "%s is not a valid duration" % duration
        raise argparse.ArgumentTypeError(msg)

    if int(duration) < 3600:
        duration = 3600
    elif int(duration) > 43200:
        duration = 43200
    return int(duration)


def log_level_arg_check(logLevel):
    """
    Check that a string is a valid Log level.

    :param param1: The Log level string to be checked.

    :returns: The Log level if the value is valid.

    :raises argparse.ArgumentTypeError: If the Log level is invalid.
    """
    if logLevel is None:
        return logging.ERROR

    origLogLevel = logLevel
    logLevel = '10' if logLevel == 'DEBUG' else logLevel
    logLevel = '20' if logLevel == 'INFO' else logLevel
    logLevel = '30' if logLevel == 'WARN' else logLevel
    logLevel = '40' if logLevel == 'ERROR' else logLevel
    logLevel = '50' if logLevel == 'CRITICAL' else logLevel

    logLevelPattern = re.compile(r'^(10|20|30|40|50)$')
    if not logLevelPattern.match(logLevel):
        msg = "%s is not a vaild Log Level" % origLogLevel
        raise argparse.ArgumentTypeError(msg)
    return int(logLevel)


def proxy_arg_check(proxyString):
    """
    Check that the input is a valid proxy string (i.e., hostname:port).

    :param param1: The proxy string to be checked.

    :returns: The proxy string if the value is valid.

    :raises argparse.ArgumentTypeError: If the proxy string is invalid.
    """
    if proxyString == 'none':
        return proxyString

    proxyPattern = re.compile(r'^(\w+\.[\w\.]+\.\w+)(:\d+)?$')
    proxyMatch = proxyPattern.match(proxyString)

    if not proxyMatch:
        msg = "%s is not a proxy string" % proxyString
        raise argparse.ArgumentTypeError(msg)

    if proxyMatch.group(1) in VZ_PROXY_LIST:
        return proxyString

    msg = "Proxy %s is not whitelisted for access" % proxyMatch.group(1)
    raise argparse.ArgumentTypeError(msg)


def username_arg_check(username):
    """
    Check that a string is a valid username.

    :param param1: The username string to be checked.

    :returns: The username if the value is valid.

    :raises argparse.ArgumentTypeError: If the username is invalid.
    """
    usernamePattern = re.compile(r'^\w{7}$')
    if not usernamePattern.match(username):
        msg = "%s argument is not a username" % username
        raise argparse.ArgumentTypeError(msg)
    return username

# Map account names. 

def reset_proxy_environment():
    """
    Reset any proxy environment variables.  awscli commands won't work without them!

    :returns: Nothing
    """
    proxyCount = 0
    for key, value in HTTP_PROXY_DICT.items():
        LOG.debug('Resetting {}'.format(key))
        proxyCount += 1
        os.environ[key] = value
    
    print(proxyCount, "Proxies Reset")

def main():
    """
    Main processing.

    """
    # Parse command-line arguments
    parser = argparse.ArgumentParser(prog='vzsaml-get-aws-credentials-auto.py',
                                     description='Get temporary AWS credentials for the user')
    parser.add_argument('--duration', type=duration_arg_check, default=3600,
                        help='Duration (in seconds) before the temporary credentials expire', required=False)
    parser.add_argument('--log_level', type=log_level_arg_check,
                        help="Logging level: DEBUG, INFO, WARN, ERROR, CRITICAL", default=40)
    parser.add_argument('--proxy', type=proxy_arg_check, default='fhdcproxy.verizon.com',
                        help='HTTP proxy for external Verizon endpoints', required=False)
    parser.add_argument('--region', default='us-east-1', help='AWS Region name', required=False)
    parser.add_argument('--section', default='vzfederate', help='Section name in credentials file', required=False)
    parser.add_argument('--username', type=username_arg_check, help='Name of user in the SSO', required=True)
    parser.add_argument('--acct', help='input account name', required=False)
    parser.add_argument('--set', action='store_true', help='Save password in MAC keychain only. Use with --username: ex: --username USER --set', required=False)
    args = parser.parse_args()

    # Setup logging
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', stream=sys.stdout, level=args.log_level)
    LOG.debug("Begin %s", 'vzsaml-get-aws-credentials-auto.py')
    LOG.debug("%s", args)
    LOG.debug("User is %s", args.username)

    if args.set is True and os.name != 'nt':
        try: 
            touchid_is_available()
            if touchid_authenticate():
                keyring.set_password('vzawsfed', args.username, getpass.getpass(prompt='\nPlease enter your AWS SSO password: '))
            else:
                LOG.error("ERROR: Authentication Failed.")
                sys.exit(1)
        except KeyError:
            print('Error saving to keyring')
            sys.exit(1)
        print("Keyring updated successfully")
        sys.exit(0)

    awsCredentialsFileName = get_credentials_file_name()

    # Log the user into the SSO and retrieve the user's SAML assertion
    unset_proxy_environment()
    loginResponse = get_login_response_from_sso(args.username)
    assertion = extract_saml_assertion(loginResponse)
    if args.acct is None:
        federatedRoleInfo = get_federated_aws_role_info(assertion)
    else:
        federatedRoleInfo = get_federated_aws_role_info_INPUT(assertion, args.acct)


    # Retrieve the temporary access keys from STS
    set_http_proxy(args.proxy)
    credentials = get_temporary_aws_credentials(federatedRoleInfo, args.region, args.duration)

    # Save the temporary access keys in the user's credentials file
    store_credentials(awsCredentialsFileName, credentials, args.region, args.section)

    # Reset the user's proxy environment so that AWS CLI commands will work.
    reset_proxy_environment()

    # Set profile environment variable for user to use Verizon
    os.environ["AWS_PROFILE"] = "vzfederate"

    return 0


# Begin
if __name__ == '__main__':
    sys.exit(main())
